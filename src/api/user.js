import { createHeaders } from "./index";

// Stores // Stores apiUrl
const apiUrl = process.env.REACT_APP_API_URL;

// Checks if there's an existing user
const userCheck = async (username) => {
  try {
    const response = await fetch(`${apiUrl}?username=${username}`);
    if (!response.ok) {
      throw new Error("Could not complete request.");
    }

    const data = await (await response).json();
    return [null, data];
  } catch (error) {
    return [error.message, null];
  }
};

//Creates a new user
const userCreate = async (username) => {
  try {
    const response = await fetch(apiUrl, {
      method: "POST",
      headers: createHeaders(),
      body: JSON.stringify({
        username,
        translations: [],
      }),
    });
    if (!response.ok) {
      throw new Error("Could not create user with username " + username);
    }

    const data = await (await response).json();
    return [null, data];
  } catch (error) {
    return [error.message, null];
  }
};

/*
1. Checks for existing user with userCheck()
2. Redirects if one exists
3. Creates a new user with userCreate() if there isn't one
*/
export const userLogin = async (username) => {
  const [checkError, user] = await userCheck(username);

  if (checkError !== null) {
    return [checkError, null];
  }

  if (user.length > 0) {
    return [null, user.pop()];
  }

  return await userCreate(username);
};

//Fetches user by Id
export const userById = async (userId) => {
  try {
    const response = await fetch(`${apiUrl}/${userId}`);

    if (!response.ok) {
      throw new Error("Could not fetch user");
    }

    const user = await response.json();
    return [null, user];
  } catch (error) {
    return [error.message, null];
  }
};
