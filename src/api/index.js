// Stores x-apy-key
const apiKey = process.env.REACT_APP_API_KEY;

// Exports header with content and x-api-key
export const createHeaders = () => {
  return {
    "content-type": "application/json",
    "x-api-key": apiKey,
  };
};
