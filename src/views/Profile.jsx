import { useEffect } from "react";
import { userById } from "../api/user";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileHistory from "../components/Profile/ProfileHistory";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const Profile = () => {
  // Takes in user context
  const { user, setUser } = useUser();

  // Finds user by id through side effect
  useEffect(() => {
    const findUser = async () => {
      const [error, latestUser] = await userById(user.id);
      if (error === null) {
        // Saves latest user to session storage
        storageSave(STORAGE_KEY_USER, latestUser);
        // Update user context
        setUser(latestUser);
      }
    };
    //findUser();
  }, [setUser, user.id]);

  return (
    <>
      <div className="container text-center p-5">
        <span className="fs-2">Profile</span>
        <ProfileHeader username={user.username} />
        <ProfileActions />
      </div>
      <div className="container text-center">
        <ProfileHistory translations={user.translations} />
      </div>
    </>
  );
};

export default withAuth(Profile);
