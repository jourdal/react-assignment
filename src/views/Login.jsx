import LoginForm from "../components/Login/LoginForm";

// Login view: displays title and login form
const Login = () => {
  return (
    <>
      <div className="container text-center p-5">
        <span className="fs-2">Login to get started</span>
        <LoginForm />
      </div>
    </>
  );
};

export default Login;
