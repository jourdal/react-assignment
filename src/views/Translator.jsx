import { useState } from "react";
import { translationAdd } from "../api/translation";
import TranslatorForm from "../components/Translator/TranslatorForm";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const Translator = () => {
  // Local state for signs
  const [signs, setSigns] = useState();
  // Takes in user context
  const { user, setUser } = useUser();

  const handleTranslateClick = async (translationInput) => {
    // Checks that something is input
    if (!translationInput) {
      alert("Please write something to be translated first");
      return;
    }

    // Adds translation to API
    const [error, updatedUser] = await translationAdd(user, translationInput);

    if (error !== null) {
      return;
    }

    // Keep UI state and server state in sync
    storageSave(STORAGE_KEY_USER, updatedUser);
    // Update user context
    setUser(updatedUser);

    // Maps and saves signImages locally, then displaying them
    const signImages = translationInput
      .toLowerCase()
      .split("")
      .map((char, idx) => (
        <img
          alt={char + idx}
          key={char + idx}
          src={"img/" + char + ".png"}
          width="50"
        />
      ));
    setSigns(signImages);
  };

  return (
    <>
      <div className="container text-center p-5">
        <span className="fs-2">Translator</span>
        <TranslatorForm onTranslate={handleTranslateClick} />
      </div>
      <div className="container text-center">
        <section className="signs">{signs}</section>
      </div>
    </>
  );
};

export default withAuth(Translator);
