import UserProvider from "./UserContext";

// App context with nested user context
const AppContext = ({ children }) => {
  return <UserProvider>{children}</UserProvider>;
};
export default AppContext;
