import { NavLink } from "react-router-dom";
import { useUser } from "../../context/UserContext";

// Basic navbar with logo, title and navlinks to translation and profile pages
const Navbar = () => {
  const { user } = useUser();

  return (
    <nav className="navbar navbar-expand-md bg-info mb-3">
      <div className="container-fluid">
        <div>
          <img src="img/Logo-Hello.png" alt="Logo-Hello" width="100" />
          <span className="fs-3 text-white align-middle">
            Lost in Translation
          </span>
        </div>
        <div className="row">
          {user !== null && (
            <div className="text-white">
              <div className="text-white">
                <NavLink to="/translator">Translator</NavLink>
              </div>
              <div>
                <NavLink to="/profile">Profile</NavLink>
              </div>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
