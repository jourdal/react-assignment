import { translationClearHistory } from "../../api/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { useUser } from "../../context/UserContext";
import { storageDelete, storageSave } from "../../utils/storage";

const ProfileActions = () => {
  // Takes in userContext
  const { user, setUser } = useUser();

  //Deletes session storage & local user upon logout click
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure?")) {
      storageDelete(STORAGE_KEY_USER);
      setUser(null);
    }
  };

  // Clears translation from API upon clear history click
  const handleClearHistoryClick = async () => {
    if (!window.confirm("Are you sure?\nThis can not be undone!")) {
      return;
    }

    const [clearError, clearResult] = await translationClearHistory(user.id);

    if (clearError !== null) {
      return;
    }

    const updatedUser = {
      ...user,
      translations: [],
    };

    // Updates translation history after clear
    storageSave(STORAGE_KEY_USER, updatedUser);
    setUser(updatedUser);
  };

  return (
    <>
      <button
        className="btn btn-warning mr-1"
        onClick={handleClearHistoryClick}
      >
        Clear history
      </button>
      <button className="btn btn-danger ml-1" onClick={handleLogoutClick}>
        Logout
      </button>
    </>
  );
};
export default ProfileActions;
