// Basic header that welcomes the user to the profile page
const ProfileHeader = ({ username }) => {
  return (
    <header className="mb-3">
      <span className="fs-5">Hello, welcome back {username} </span>
    </header>
  );
};
export default ProfileHeader;
