// List elements of translation history
const ProfileHistoryItem = ({ translation }) => {
  return <p>{translation}</p>;
};
export default ProfileHistoryItem;
