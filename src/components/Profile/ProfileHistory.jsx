import ProfileHistoryItem from "./ProfileHistoryItem";

// Lists translations of user on profile page
const ProfileHistory = ({ translations }) => {
  const translationList = translations.map((translation, index) => (
    <ProfileHistoryItem
      key={index + "-" + translation}
      translation={translation}
    />
  ));

  return (
    <section>
      <span className="fs-5">Your translation history:</span>

      {translationList.length === 0 && <p>You have no translations yet</p>}

      {translationList}
    </section>
  );
};
export default ProfileHistory;
