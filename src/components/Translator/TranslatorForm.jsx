import { useForm } from "react-hook-form";

const TranslatorForm = ({ onTranslate }) => {
  // Hooks
  const { register, handleSubmit } = useForm();

  // Links button press with translation input
  const onSubmit = ({ translationInput }) => {
    onTranslate(translationInput);
  };

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <div className="input-group mb-3 mt-3">
            <input
              type="text"
              {...register("translationInput")}
              placeholder="Write text here"
              className="form-control"
              aria-label="Write text here"
              aria-describedby="button-addon2"
            />
            <button
              className="btn btn-primary"
              type="submit"
              id="button-addon2"
            >
              Translate
            </button>
          </div>
        </fieldset>
      </form>
    </>
  );
};
export default TranslatorForm;
