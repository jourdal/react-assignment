import { useState, useEffect } from "react";
import { useForm } from "react-hook-form";
import { userLogin } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

// Config for username input: input is required & min length of username must be 3 or greater
const usernameConfig = {
  required: true,
  minLength: 3,
};

const LoginForm = () => {
  // Hooks
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  // Takes in userContext and navigation
  const { user, setUser } = useUser();
  const navigate = useNavigate();

  // Local State
  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  // Navigates to translation page if a user is already logged in
  useEffect(() => {
    if (user !== null) {
      navigate("translator");
    }
  }, [user, navigate]);

  // Logs in user and stores it in session storage
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await userLogin(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  // Checks input for valid username
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }

    if (errors.username.type === "required") {
      return <span>Username is required</span>;
    }

    if (errors.username.type === "minLength") {
      return <span>Username is too short (min 2 characters)</span>;
    }
  })();

  return (
    <>
      <div className="container">
        <div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="input-group mb-3 mt-3">
              <input
                type="text"
                {...register("username", usernameConfig)}
                placeholder="What's your name?"
                className="form-control"
                aria-label="What's your name?"
                aria-describedby="button-addon2"
              />
              <button
                className="btn btn-success"
                type="submit"
                id="button-addon2"
                disabled={loading}
              >
                Continue
              </button>
            </div>
          </form>
        </div>
        <div>
          {errorMessage}
          {loading && <p>Logging in...</p>}
          {apiError && <p>{apiError}</p>}
        </div>
      </div>
    </>
  );
};

export default LoginForm;
