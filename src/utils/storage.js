// Validates the storage key
const validateKey = (key) => {
  if (!key || typeof key !== "string") {
    throw new Error("storageSave: Invalid storage key provided");
  }
};

// Saves session storage
export const storageSave = (key, value) => {
  validateKey(key);
  if (!value) {
    throw new Error("storageSave: No value provided for " + key);
  }

  sessionStorage.setItem(key, JSON.stringify(value));
};

// Reads session storage
export const storageRead = (key) => {
  validateKey(key);
  const data = sessionStorage.getItem(key);
  if (data) {
    return JSON.parse(data);
  }

  return null;
};

// Deletes session storage 
export const storageDelete = (key) => {
  validateKey(key);
  sessionStorage.removeItem(key);
};
