# Lost in Translation - sign language translator using React and ContextAPI

This project was made in collaboration with Noroff.

## Table of contents
1. [Installation](#installation)
2. [Usage](#usage)
3. [Author](#author)

## Installation

Guides on installing dependencies for this project. E.g. for a node project

```bash
npm install
```

## Usage

How to start running the project

```bash
npm start
```

## Author

Jo Endre Brauten Urdal
